package code;
import code.StdIn;
import code.StdOut;

/*************************************************************************
 *  Compilation:  javac Element.java
 *  Execution:    java Element < elements.csv
 *  Dependencies: StdIn.java StdOut.java
 *                http://www.cs.princeton.edu/introcs/data/elements.csv
 *  
 *  Data type for elements in periodic table. Store name, atomic number,
 *  symbol, and atomic.
 *
 *************************************************************************/

public class Element {
    private String name;       // name of element
    private int    number;     // number in periodic table
    private String symbol;     // atomic symbol
    private double weight;     // atomic weight 

    public Element(String name, int number, String symbol, double weight) {
        this.name   = name;
        this.number = number;
        this.symbol = symbol;
        this.weight = weight;
    }


    public String toString() {
        String s = "";
        s = s +  name   + " (" + symbol + ")\n";
        s = s + "Atomic number: " + number + "\n";
        s = s + "Atomic weight: " + weight + "\n";
        return s;
    }
    
    public static void main(String[] args) {
        int ELEMENTS = 103;
        Element[] elements = new Element[ELEMENTS];

        // ignore first line
        String s = StdIn.readLine();

        // read data
        for (int i = 0; i < ELEMENTS; i++) {
            s = StdIn.readLine();
            String[] fields = s.split(",");
            String name   = fields[0];
            int number    = Integer.parseInt(fields[1]);
            String symbol = fields[2];
            double weight = Double.parseDouble(fields[3]);
            elements[i] = new Element(name, number, symbol, weight);
            StdOut.println(elements[i]);
        }
    }
}
 

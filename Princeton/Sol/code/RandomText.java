package code;

/*************************************************************************
 *  Compilation:  javac RandomText.java
 *  Execution:    java RandomText s N
 *  Dependencies: StdDraw.java
 *
 *  Plots the String s, N times at random locations, and in random
 *  colors.
 *
 *  % java RandomText Hello 10
 *
 *  % java RandomText World 15
 *
 *  % java RandomText Java 20
 *
 *
 *************************************************************************/

import java.awt.Color;
import java.awt.Font;

import code.StdDraw;

public class RandomText { 

    public static void main(String[] args) { 
        String s = args[0];
        int N = Integer.parseInt(args[1]);
        StdDraw.clear(Color.BLACK);
        Font f = new Font("Arial", Font.BOLD, 60);
        StdDraw.setFont(f);

        for (int i = 0; i < N; i++) {
            StdDraw.setPenColor(Color.getHSBColor((float) Math.random(), 1.0f, 1.0f));
            double x = Math.random();
            double y = Math.random();
            StdDraw.text(x, y, s);
        }
    }

}

package code;
/*************************************************************************
 *  Compilation:  javac Distinct.java
 *  Execution:    java Distinct M N T
 *  
 *  Perform T trials of the following experiment: Generate N random int
 *  values between 0 and M – 1 and count the number of distinct values
 *  generated.
 *
 *  Probability theory says that the number of distinct values should
 *  be about M(1 – e^(-alpha)), where alpha = N/M.
 *
 *************************************************************************/

import java.util.Arrays;

import code.StdOut;
import code.StdRandom;
import code.StdStats;

public class Distinct {

    // return number of distinct entries in array a[]
    public static int distinct(Comparable[] a) {
        if (a.length == 0) return 0;
        Arrays.sort(a);
        int distinct = 1;
        for (int i = 1; i < a.length; i++)
            if (a[i].compareTo(a[i-1]) != 0)
                distinct++;
        return distinct;
    }

    public static void main(String[] args) {
        int M = Integer.parseInt(args[0]);
        int N = Integer.parseInt(args[1]);
        int T = Integer.parseInt(args[2]);


       int[] distinct = new int[T];
       for (int trial = 0; trial < T; trial++) {
           Integer[] a = new Integer[N];
           for (int i = 0; i < N; i++) {
               a[i] = StdRandom.uniform(M);
           }
           distinct[trial] = distinct(a);
       }

       double empirical = StdStats.mean(distinct);
       double alpha = (double) N / M;
       double theoretical = M * (1 - Math.exp(-alpha));
       StdOut.printf("Theoretical = %.3f\n", theoretical);
       StdOut.printf("Empirical   = %.3f\n", empirical);
    }
}

package code;
/*************************************************************************
 *  Compilation:  javac LatinSquare.java
 *  Execution:    java LatinSquare N
 * 
 *  % java LatinSquare 5
 *  A B C D E 
 *  B C D E A 
 *  C D E A B 
 *  D E A B C 
 *  E A B C D 
 *
 *  Limitations
 *  -----------
 *   - N is at most 26
 *
 *************************************************************************/

public class LatinSquare {

    public static void main(String[] args) { 
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        int N = Integer.parseInt(args[0]);
        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                char c = alphabet.charAt((i + j) % N);
                System.out.print(c + " ");
            }
            System.out.println();
        }
    }
}

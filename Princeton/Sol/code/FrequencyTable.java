package code;
import code.ST;
import code.StdIn;
import code.StdOut;

/*************************************************************************
 *  Compilation:  javac FrequencyTable StdIn.java
 *  Execution:    java FrequencyTable < words.txt
 *
 *  Read in a list of words from standard input and print out
 *  each word and the number of times it appears.
 *
 *  % java Frequency < mobydick.txt | sort -rn | more
 *  13967 the
 *  6415 of
 *  6247 and
 *  4583 a
 *  4508 to
 *  4037 in
 *  2911 that
 *  2481 his
 *
 *************************************************************************/

public class FrequencyTable<Key extends Comparable<Key>> {
    private ST<Key, Integer> st = new ST<Key, Integer>();

    // add 1 to the number of times key appears
    public void hit(Key key) {
        Integer freq = st.get(key);
        if (freq == null) st.put(key, 1);
        else              st.put(key, freq + 1);
    }

    // return the number of times the key appears
    public int count(Key key) {
        Integer freq = st.get(key);
        if (freq == null) return 0;
        else              return freq;
    }

    // print all the keys to standard output
    public void show() {
        for (Key key : st.keys())
            StdOut.println(st.get(key) + " " + key);
    }


    public static void main(String[] args) {
        FrequencyTable<String> f = new FrequencyTable<String>();
        while (!StdIn.isEmpty()) {
            String key = StdIn.readString();
            f.hit(key);
        }
        f.show();


    }
}


package code;
import code.StdOut;

/*************************************************************************
 *  Compilation:  javac Student.java
 *  Execution:    java Student
 *  
 *  Illustrates implementation of equals.
 *
 *************************************************************************/

public class Student {
   
    private String name;
    private int section;

    public Student(String name, int section) {
        this.name    = name;
        this.section = section;
    }

    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        Student b = (Student) y;
        Student a = this;
        return (a.section == b.section) && (a.name.equals(b.name));
    }




    // test client
    public static void main(String[] args) {
        Student alice = new Student("Alice", 3);
        Student bob   = new Student("Bob",   3);
        Student ali   = new Student("Alice", 3);
        String fred = "Fred";

        StdOut.println("alice == ali:        " + (alice == ali));
        StdOut.println("alice.equals(ali):   " + (alice.equals(ali)));
        StdOut.println("alice.equals(bob):   " + (alice.equals(bob)));
        StdOut.println("alice.equals(fred):  " + (alice.equals(fred)));
    }

}

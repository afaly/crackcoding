package code;
import code.StdIn;
import code.StdOut;


/*************************************************************************
 *  Compilation:  javac Add.java
 *  Execution:    java Add
 *  Dependencies: StdIn.java
 *  
 *  Prompts the user for two integers numbers, reads them in from standard
 *  input, and prints out their sum.
 *
 *  Note: you must hav the file StdIn.java in your working directory.
 *
 *  % java Add
 *  Type the first integer
 *  5                                 <--  user types this
 *  Type the second integer
 *  33                                <--  user types this
 *  Their sum is 38
 *
 *************************************************************************/

public class Add {
    public static void main(String[] args) {

        // prompt user for first integer and read from standard input
        StdOut.println("Type the first integer");
        int x = StdIn.readInt();

        // prompt user for second integer and read from standard input
        StdOut.println("Type the second integer");
        int y = StdIn.readInt();

        // compute sum and print to standard output
        int sum = x + y;
        StdOut.println("Their sum is " + sum);
   }
}

package code;
/**  
 *  
 * @author Dmitry Leskov  
 */

import java.lang.reflect.Array;

public class GenericArrayFactory<Item> {  

    public Item[] ArrayOf(Class<Item[]> clazz, int length) {  
        return clazz.cast(Array.newInstance(clazz.getComponentType(), length));  
    }

    public static void main(String[] args) {  
        Integer[] intArray = new GenericArrayFactory<Integer>().ArrayOf(Integer[].class, 42);  
    }  
}

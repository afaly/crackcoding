package code;
import code.StdIn;


/*************************************************************************
 *  Compilation:  javac Ruler.java
 *  Execution:    echo 1 1 | java Ruler | java Ruler | java Ruler
 *  
 *  Prints the relative lengths of the subdivisions on a ruler or
 *  order N.
 * 
 *  % echo 1 1 | java Ruler 
 *  2 121
 *
 *  % echo 1 1 | java Ruler | java Ruler
 *  3 1213121
 *
 *  % echo 1 1 | java Ruler | java Ruler | java Ruler
 *  4 121312141213121 
 *
 *  % echo 1 1 | java Ruler | java Ruler | java Ruler | java Ruler
 *  5 1213121412131215121312141213121 
 *
 *************************************************************************/

public class Ruler { 
   public static void main(String[] args) { 
      int n = StdIn.readInt();
      String s = StdIn.readString();
      System.out.println((n+1) + " " + s + (n+1)  + s);
   }

}

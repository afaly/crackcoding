package code;
/*************************************************************************
 *  Compilation:  javac Bug.java
 *  Execution:    java Bug
 *  
 *  Uses reflection to crash the Java Virtual Machine. Appears to
 *  work with Java 1.4.2_05 on Windows, Linux, Solaris, and OS X.
 *
 *  Written by Daniel Wang.
 *
 *  % java Bug
 *  Bus error
 *
 *************************************************************************/

import java.lang.reflect.Field;
import java.util.zip.Deflater;

public class Bug {
    public static void main(String args[]) {
        Deflater deflate    = new Deflater();
        byte[] in_buffer    = new byte[1024];
        byte[] out_buffer   = new byte[1024];
        Class deflate_class = deflate.getClass();
        try {
            Field strm = deflate_class.getDeclaredField("strm");
            strm.setAccessible(true);
            strm.setLong(deflate, 1L);
        } catch (Exception e) {
            System.err.println("bug:" + e);
        }
        deflate.setInput(in_buffer);
        deflate.deflate(out_buffer);
    }
}

package code;
/*************************************************************************
 *  Compilation:  javac ColorJulia.java
 *  Execution:    java ColorJulia a b
 *  Dependencies: Picture.java
 *  
 *  Plots the Julia for the complex point c = a + ib.
 *
 *  The set of points in the Julia set is connected if and only if
 *  c is in the Mandelbrot set.
 *
 *  % java ColorJulia -0.75 0.1 
 *
 *  % java ColorJulia -1.25 0
 *
 *  % java ColorJulia 0.1 0.7
 *
 *************************************************************************/

import java.awt.Color;

import code.Picture;
import code.StdIn;

public class ColorJulia {


    // return number of iterations to check z is in the Julia set of c
    static int julia(Complex c, Complex z, int ITERS) {
        for (int t = 0; t < ITERS; t++) {
            if (z.abs() > 2.0) return t;
            z = z.times(z).plus(c);
        }
        return ITERS - 1;
    }


    public static void main(String[] args) {
        double real = Double.parseDouble(args[0]);      // a
        double imag = Double.parseDouble(args[1]);      // b
        Complex c = new Complex(real, imag);            // c = a + ib
        double xmin   = -2.0;
        double ymin   = -2.0;
        double width  =  4.0;
        double height =  4.0;

        int N = 512;
        int ITERS  = 256;

        // read in color map
        Color[] colors = new Color[ITERS];
        for (int t = 0; t < ITERS; t++) {
            int r = StdIn.readInt();
            int g = StdIn.readInt();
            int b = StdIn.readInt();
            colors[t] = new Color(r, g, b);
        }
        Picture pic = new Picture(N, N);

        for (int i = 0; i < N; i++) {
            for (int j = 0; j < N; j++) {
                double x = xmin + i * width / N;
                double y = ymin + j * height / N;
                Complex z = new Complex(x, y);
                int t = julia(c, z, ITERS);
                pic.set(i, j, colors[t]);
            }
        }
        pic.show();
    }

}
